package testNGTestsCRM;

import java.io.FileInputStream;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity_13Excel {
	WebDriver driver;
	WebDriverWait wait;
	String filePath = "src/Files/Book1.xlsx";

	ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
		public Boolean apply(WebDriver driver) {
			return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString()
					.equals("complete");
		}
	};

	@BeforeClass
	public void beforeClass() {
		// Create a new instance of the Firefox driver
		driver = new FirefoxDriver();

		// Open browser
		driver.get("http://alchemy.hguy.co/crm");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		wait = new WebDriverWait(driver, 10);
	}

	@Test(priority = 0)
	public void Login() {
		driver.findElement(By.xpath("//input[@id='user_name']")).sendKeys("admin");
		driver.findElement(By.xpath("//input[@id='username_password']")).sendKeys("pa$$w0rd");
		driver.findElement(By.xpath("//input[@id='bigbutton']")).click();

	}

	@Test(priority = 1)
	public void gotoproduct() throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;

		driver.findElement(By.xpath("//a[@id='grouptab_5']")).click();
		// Thread.sleep(10000);

		js.executeScript("arguments[0].scrollIntoView();",
				driver.findElement((By.xpath("//span[@class='notCurrentTab']//ul//li[25]"))));
		driver.findElement(By.xpath("//span[@class='notCurrentTab']//ul//li[25]")).click();

	}

	@Test(priority = 2)
	public void addproduct() {
		// Read Excel
		try {

			FileInputStream file = new FileInputStream(filePath);
			// Create Workbook instance holding reference to Excel file
			XSSFWorkbook workbook = new XSSFWorkbook(file);
			// Get first sheet from the workbook
			XSSFSheet sheet = workbook.getSheetAt(0);
			// Iterate through each rows one by one

			Iterator<Row> rowIterator = sheet.iterator();
			Row row = rowIterator.next();

			while (rowIterator.hasNext()) {

				row = rowIterator.next();
				// For each row, iterate through all the columns
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {

					Cell cell = cellIterator.next();

					wait.until(ExpectedConditions
							.elementToBeClickable(By.xpath("//div[@id='actionMenuSidebar']/ul/li[1]"))).click();
					// driver.findElement(By.xpath("//div[@id='actionMenuSidebar']/ul/li[1]")).click();
					 Thread.sleep(5000);
					wait.until(expectation);
					//driver.findElement(By.xpath("//input[@id='name']")).clear();
					wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='name']")))
							.sendKeys(cell.toString());
					cell = cellIterator.next();
					wait.until(expectation);
					wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='price']"))).clear();
					driver.findElement(By.xpath("//input[@id='price']")).sendKeys(cell.toString());
					cell = cellIterator.next();
					wait.until(expectation);
					wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//textarea[@id='description']")))
							.clear();
					
					driver.findElement(By.xpath("//textarea[@id='description']")).sendKeys(cell.toString());
					// Thread.sleep(2000);
					// driver.findElement(By.xpath("//input[@id='SAVE']")).click();
					Thread.sleep(10000);
					wait.until(expectation);
					wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='SAVE']"))).click();
					wait.until(expectation);
				}

			}

			file.close();

			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test(priority = 3)
	public void productverification() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='actionMenuSidebar']//ul/li[2]")))
				.click();
		 Thread.sleep(3000);
		 wait.until(expectation);
		String name = driver
				.findElement(By.xpath("//div[@class='list-view-rounded-corners']/table/tbody/tr[1]/td[3]//a"))
				.getText();
		Assert.assertEquals("LG", name);

	}
}
